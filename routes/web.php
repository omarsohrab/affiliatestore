<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'BlogController@index')->name('blog.index');
Route::get('post/{slug}', 'BlogController@show')->name('post.show');
Route::get('/stories/{slug}','BlogController@categoryStories')->name('stories');
Route::get('/author/{name}','BlogController@authorsStories')->name('author.stories');
Route::get('/story/featured','BlogController@featuredStories')->name('featured.stories');
Route::post('/search', 'BlogController@search');


Route::get('/test/{id}', function ($id) {
    $ca = App\Category::get();
    $a = App\Post::get($id)->author_name;
    $childs = App\Category::getChilds($id);
    return view('welcome',compact('ca'));
    //return count($childs);
});



Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
