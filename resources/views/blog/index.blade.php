@extends('layouts.master')
<!-- 1: TITLE OF SITE -->
@section('title', 'Travelers best friend')

<!-- 2: Meta -->   
@section('meta')
    <meta name="description" content="travel blog plus plus" />
    <meta name="keywords" content="app, landing page, gradient background, image background, video background, template, responsive, bootstrap" />
       
@endsection <!--end: meta section --> 

<!--
      |========================
    3 |  HEADER
      |========================
-->
@section('header')
    {{--  this header is with mobile menu and search bar  --}}
    <header id="xt-home" class="xt-header">
        @php
            $flag = 'home';
            $categories = \App\Category::get();
        @endphp

        @include('headers.navbar')
            
        @include('headers.searchNavWithWrapper')
        {{--  @include('parts.onlySearchNav')  --}}

    </header>
    <!--Mobile Menu-->
        @include('headers.mobileMenu')

@endsection <!--end: header section --> 

<!--
    |========================
  4 |  Page Content
    |========================
    -->
@section('content')
    <section class="xt-slider">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 category-hidden padding-right-o"></div>
                    <div class="col-md-9 col-md-offset-3 padding-o padding-sm">
                     
                            
                            <div class="xt-blog col-xs-12">
                            @foreach($featureds as $key => $post)
                                <div class="col-md-4 col-sm-4">
                                    <div class="xt-grid-post">
                                        <img src="{{ Voyager::image( $post->image ) }}" alt="" class="img-responsive">
                                        <div class="grid-content">
                                            <span>{{ $post->created_at }}</span>
                                            <a href="/post/{{ $post->slug }}"><h3>{{str_limit( $post->title,50) }}</h3></a>
                                            <p>{{str_limit($post->excerpt,100) }}</p>
                                        </div>
                                    </div>
                                </div>
                                @if(($key+1) % 3 == 0)
                                    <div class="row"></div>
                                @endif
                            @endforeach 
                                
                            </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-10 col-md-offset-4 col-sm-offset-1">
                                <a href="{{route('featured.stories')}}" class="btn btn-fill">Read More Featured Stories</a>
                            </div>
                        </div>
                           
                    </div>
                </div>
            </div>
            
        </section>
        
        
        
        
        <!--
        |========================
        |  BLOG
        |========================
        -->
        <section class="xt-blog">
            <div class="container">
                <div class="row section-separator">
                    <div class="section-title">
                        <h2>Latest blog Post</h2>
                        <span class="xt-title-bg"></span>
                    </div>
                    <div class="xt-blog col-xs-12">
                        
                        @foreach($posts as $key => $post)
                            <div class="col-md-4 col-sm-4">
                                <div class="xt-grid-post">
                                    <img src="{{ Voyager::image( $post->image ) }}" alt="" class="img-responsive">
                                    <div class="grid-content">
                                        <span>{{ $post->created_at }}</span>
                                        <a href="/post/{{ $post->slug }}"><h3>{{str_limit( $post->title,50) }}</h3></a>
                                        <p>{{str_limit($post->excerpt,100) }}</p>
                                    </div>
                                </div>
                            </div>
                            @if(($key+1) % 3 == 0)
                                <div class="row"></div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
        
        <!--
        |========================
        |  SUBSCRIBE
        |========================
        -->
        <div class="black-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <form class="form-inline xt-subscribe-form">
                            <div class="form-group col-xs-10 xt-subscribe">
                                <label for="subscribe">Subscribe</label>
                                <input type="text" class="form-control" id="subscribe" placeholder="Your email address">
                            </div>
                            <div class="col-md-2 col-xs-2">
                                <button type="submit" class="btn btn-fill"><i class="fa flaticon-home"></i></button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="xt-social">
                            <span>stay conected</span>
                            <ul>
                                <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                                <li><a href=""><i class="fa fa-dribbble"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection <!--end: content section --> 

<!--
        |========================
  5     |  FOOTER
        |========================
-->
@section('footer')
    @include('parts.footer')
@endsection  <!--end: footer section --> 