@extends('layouts.master')
<!-- 1: TITLE OF SITE -->
@section('title', $post->title )

@section('css')
    <link rel="stylesheet" href="{{asset('assets/css/blog.css')}}">
@endsection

<!-- 2: Meta -->   
@section('meta')
    <meta name="description" content="travel blog plus plus" />
    <meta name="keywords" content="app, landing page, gradient background, image background, video background, template, responsive, bootstrap" />
       
@endsection <!--end: meta section --> 

<!--
      |========================
    3 |  HEADER
      |========================
-->
@section('header')
    <header id="xt-home" class="xt-header">
        @php
            $flag = null;
            $categories = \App\Category::get();
        @endphp
        
        @include('headers.navbar')
            <!--Mobile Menu-->
        {{--  @include('parts.searchNavWithWrapper')  --}}
        @include('headers.onlySearchNav')

    </header>
@endsection <!--end: header section --> 

<!--s
    |========================
  4 |  Page Content
    |========================
    -->
@section('content')
 
    <br>
    <section>
        <div class="container">
            <div class="row">
                
                <div class="col-md-9  padding-o padding-sm">
                    <div >
                        <img alt="" class="img-responsive" id = "main-image" src="{{ Voyager::image( $post->image ) }}" />
                        <h2>{{$post->title}}</h2>
                        <ul class="post-meta-group">
                            <li><i class="fa fa-user"></i><a href="{{route('author.stories',$post->author->name)}}"> {{ $post->author->name}}</a></li>
                            <li><i class="fa fa-clock-o"></i><time> {{$post->created_at}}</time></li>
                            <li><i class="fa fa-tags"></i><a href="{{route('stories',$post->category->slug)}}"> {{$post->category->name}}</a></li>
                            
                        </ul>
                        <hr class="style6">
                        <br>
                    </div>
                    <div class="richtext">
                    <p>{!! $post->body !!}</p>
                    </div>
                </div>
                <div class="col-md-3">
                    
                    <h3>Relevent Stories</h3>
                    
                    @foreach($posts as $post)
                    <hr class="style5">
                        <div class="row">
                            <div class="col-xs-12">
                                <img src="{{ Voyager::image( $post->image ) }}" alt="" class="img-responsive">
                                <div class="grid-content">
                                    
                                    <a href="/post/{{ $post->slug }}"><h4>{{str_limit( $post->title,50) }}</h4></a>
                                    <span>{{ $post->created_at }}</span>
                                    
                                </div>
                            </div>
                        </div>
                        
                    @endforeach 
                </div>
            </div>
        </div>
    </section>
@endsection <!--end: content section --> 

<!-- 
        |========================
  5     |  FOOTER
        |========================
-->
@section('footer')
    @include('parts.footer')
@endsection  <!--end: footer section --> 