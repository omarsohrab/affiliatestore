<div class="main-color-bg">
    <div class="container">
        <div class="row">
            @include('headers.blogCategoryNav')
            
            <div class="col-md-8 col-sm-10 col-xs-12 xt-header-search">
                @include('headers.blogSearch')
            </div>
            
        </div>
    </div>
</div>