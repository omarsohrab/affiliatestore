<form method="post" action="/search">
        {{ csrf_field() }}
    <div class="form-group xt-form search-bar  col-md-8 col-sm-8 col-xs-7 padding-right-o">
        <input type="text" name="keyword" class="form-control" placeholder="Search for stories" />
    </div>
    <div class="form-group xt-form xt-search-cat col-md-4 col-sm-4 col-xs-5 padding-left-o ">
        <div class="xt-select xt-search-opt">
            <select name="category" class="xt-dropdown-search select-beast">
                <option>All Categories</option>
                
                @foreach($categories as $item)
                <option>{{$item->name}}</option> 
                @endforeach
                
                
            </select>
        </div>
        <div class="xt-search-opt xt-search-btn">
            <button type="submit" class="btn btn-primary btn-search"><i class="fa fa-search"></i></button>
        </div>
    </div>
</form>