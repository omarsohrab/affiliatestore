<div class="menu-spacing">
    <div class="container">
        <div class="row">
            <div class="mobile-menu-area visible-xs visible-sm">
                <div class="mobile-menu">
                    <nav id="mobile-menu-active">

                            
                        
                        <ul class="main">
                            @foreach($categories as $item)
                                @php
                                    $childs = App\Category::getChilds($item->id);
                                @endphp  
                                @if($childs)
                                <li class="active"><a class="main-a" href="/stories/{{$item->slug}}">{{$item->name}}</a>
                                    <ul>
                                        @foreach($childs as $child)
                                        <li><a href="/stories/{{$child->slug}}">{{$child->name}}</a></li>
                                        @endforeach
                                    </ul>	
                                </li>
                                @elseif($item->order==1)
                                     <li><a class="main-a" href="/stories/{{$item->slug}}">{{$item->name}}</a></li>
                                @endif
                                    
                            @endforeach
                        </ul>
                    </nav>
                </div>	
            </div>
        </div>
    </div>
</div>