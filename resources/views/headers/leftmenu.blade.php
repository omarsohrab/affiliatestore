<div class="col-md-3 left-menu-wrapper">
    <div class="xt-sidenav hidden-xs hidden-sm">
        <nav>
            <ul class="xt-side-menu">
                <li>
                    <a href="#">All Category</a>
                    <ul class="xt-dropdown">
                        
                        <li><a href="single-shop.html"><i class="fa flaticon-high-heel"></i> SHOES</a></li>
                        <li>
                            <a class="xt-nav-link" href="single-shop.html"><i class="fa flaticon-v-neck-shirt"></i> MAN'S</a>
                            <ul class="mega-menu xt-column">
                                <li>
                                    <ul class="xt-single-mega">
                                        <li><a href="single-shop.html">Jeans Pant</a></li>
                                        <li><a href="single-shop.html">T-Shirts</a></li>
                                        <li><a href="single-shop.html">Pant</a></li>
                                        <li><a href="single-shop.html">Jackets</a></li>
                                        <li><a href="single-shop.html">Cap</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="single-shop.html"><i class="fa flaticon-jacket"></i> JACKETS</a></li>
                        <li>
                            <a class="xt-nav-link" href="single-shop.html"><i class="fa flaticon-cosmetics"></i> COSMETICS</a>
                            <ul class="mega-menu xt-column">
                                <li>
                                    <ul class="xt-single-mega">
                                        <li><a href="single-shop.html">Lipstick</a></li>
                                        <li><a href="single-shop.html">Makeup Brush</a></li>
                                        <li><a href="single-shop.html">Nail Polish</a></li>
                                        <li><a href="single-shop.html"> Hair Dryers</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="xt-nav-link" href="single-shop.html"><i class="fa flaticon-hijab"></i>BOURKHA & HIJAB</a>
                            <ul class="mega-menu xt-column">
                                <li>
                                    <ul class="xt-single-mega">
                                        <li><a href="single-shop.html">Iranian</a></li>
                                        <li><a href="single-shop.html">Arabian</a></li>
                                        <li><a href="single-shop.html">Indian</a></li>
                                        <li><a href="single-shop.html">Indonesian</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="xt-nav-link" href="single-shop.html"><i class="fa flaticon-jacket-1"></i>Coats & Gilets</a>
                            <ul class="mega-menu xt-column">
                                <li>
                                    <ul class="xt-single-mega">
                                        <li><a href="single-shop.html">German Coats</a></li>
                                        <li><a href="single-shop.html">Bangladeshi Blezzar</a></li>
                                        <li><a href="single-shop.html">Thailand Gilets</a></li>
                                        <li><a href="single-shop.html">Indian Coats</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="single-shop.html"><i class="fa flaticon-dress-1"></i>Boutique</a></li>
                        <li><a href="#more-list"> <i class="fa flaticon-menu"></i> More Categories</a></li> 
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>