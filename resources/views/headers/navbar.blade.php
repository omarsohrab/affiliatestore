<div class="main-navigation">
    <nav class="navbar navbar-fixed-top nav-scroll">
        <div class="container">
            <div class="row">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span aria-hidden="true" class="icon"></span>
                    </button>
                    <a class="navbar-brand" href="{{route('blog.index')}}"><img src="{{asset('assets/images/flogo.png')}}" alt="" class="img-responsive"></a>
                </div>
                
                <div class="collapse navbar-collapse" id="js-navbar-menu">
                    <ul class="nav navbar-nav navbar-right ep-mobile-menu" id="navbar-nav">
                        <li @if($flag=='home')
                            class="active"
                        @endif ><a href="{{route('blog.index')}}">Home</a></li>


                        <li @if($flag=='shop')
                            class="active"
                        @endif ><a href="shop-page.html">Travel Products</a></li>
                        
                        <li @if($flag=='compare')
                            class="active"
                        @endif ><a href="single-shop.html">Compare</a></li>

                        <li @if($flag=='about')
                            class="active"
                        @endif ><a href="">About</a></li>
                        
                        <li @if($flag=='blog')
                            class="active"
                        @endif ><a href="">Stories</a></li>

                        <li @if($flag=='contact')
                            class="active"
                        @endif ><a href="contact-us.html">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</div>