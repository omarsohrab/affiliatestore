<div class="col-md-3 left-menu-wrapper">
    <div class="xt-sidenav hidden-xs hidden-sm">
        <nav>
            <ul class="xt-side-menu">
                <li>
                    <a href="#">All Category</a>
                    <ul class="xt-dropdown">
                        
                        @foreach($categories as $item)
                            @php
                                $childs = App\Category::getChilds($item->id);
                            @endphp  
                            @if($childs)
                                
                                <li>
                                    <a class="xt-nav-link" href="/stories/{{$item->slug}}"><i class="fa fa-paper-plane-o"></i>{{$item->name}}</a>
                                    <ul class="mega-menu xt-column">
                                        <li>
                                            <ul class="xt-single-mega">
                                                @foreach($childs as $child)
                                                    <li><a href="/stories/{{$child->slug}}">{{$child->name}}</a></li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            @elseif($item->order==1)
                                <li><a href="/stories/{{$item->slug}}"><i class="fa fa-paper-plane-o"></i> {{$item->name}}</a></li>
                            @endif
                                
                        @endforeach
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>