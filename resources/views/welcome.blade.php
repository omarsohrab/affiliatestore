@extends('layouts.master')
<!-- 1: TITLE OF SITE -->
@section('title', 'Travelers best friend')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/css/blog.css')}}">
@endsection

<!-- 2: Meta -->   
@section('meta')
    <meta name="description" content="travel blog plus plus" />
    <meta name="keywords" content="app, landing page, gradient background, image background, video background, template, responsive, bootstrap" />
       
@endsection <!--end: meta section --> 

<!--
      |========================
    3 |  HEADER
      |========================
-->
@section('header')
     <header id="xt-home" class="xt-header">
        @php
            $flag = 'blog';
            $categories = \App\Category::get();
        @endphp

        @include('headers.navbar')
            
        {{--  @include('headers.searchNavWithWrapper')  --}}
        @include('headers.onlySearchNav')

    </header>
    <!--Mobile Menu-->
        

@endsection <!--end: header section --> 

<!--s
    |========================
  4 |  Page Content
    |========================
    -->
@section('content')
    
    <section class="xt-slider">
        <div class="container">
        <br>
            <div class="row">
            <div class="col-lg-12 col-md-12 mx-auto">
            @if(count($posts)<1)
                <div class="alert alert-danger" style="text-align:center">
                    <h2><strong>Opps :( </strong> <br> Not Stories found of your interest. 
                    <br> Please Search again</h2>
                </div>
                
            @else
            @foreach($posts as $post)
                <div class="row">
                    <div class="panel panel-default">
                       
                        <div class="panel-heading">
                             <a href="/post/{{ $post->slug }}">
                            <h3 class="">
                                    {{$post->title }}
                            </h3>
                            </a>
                        </div>
                        <div class="panel-body">
                            <div class="col-sm-2">
                                <img alt="" class="img-responsive"  src="{{ Voyager::image( $post->image ) }}" />
                            </div>
                            <div class="col-sm-10">
                            
                                
                                <p class="post-subtitle">
                                {{str_limit($post->excerpt,400) }}
                                </p>
                            
                                <ul class="post-meta-group">
                                    <li><i class="fa fa-user"></i><a href="{{route('author.stories',$post->author->name)}}"> {{ $post->author->name}}</a></li>
                                    <li><i class="fa fa-clock-o"></i><time> {{$post->created_at}}</time></li>
                                    <li><i class="fa fa-tags"></i><a href="{{route('stories',$post->category->slug)}}"> {{$post->category->name}}</a></li>
                                    
                                </ul>
                            </div>
                            
                        </div>
                        

                    </div>
                </div>
                
            @endforeach   
            {{ $posts->links() }} 
            @endif
            </div>
            </div>
        </div>
        
    </section>
@endsection <!--end: content section --> 

<!-- 
        |========================
  5     |  FOOTER
        |========================
-->
@section('footer')
    @include('parts.footer')
@endsection  <!--end: footer section --> 