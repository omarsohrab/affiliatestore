<!DOCTYPE html>

<!--[if IE 7]> <html class="no-js ie7 oldie" lang="en-US"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8 oldie" lang="en-US"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
    <head>
        <!-- TITLE OF SITE -->
        <title>Safe Travels | @yield('title') </title>
        
        <!-- Meta -->
        <meta charset="utf-8">
        <meta name="description" content="app landing page template" />

        @yield('meta')

        <meta name="developer" content="Omar Sohrab | omar.sohrab@gmail.com">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        
        <!-- FAV AND ICONS   -->
        
        
        <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/icons/screen.png')}}">
        

        <!-- GOOGLE FONTS -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700%7cPlayfair+Display:400,400i,700,900" rel="stylesheet">

        <!-- FONT ICONS -->
        <link rel="stylesheet" href="{{asset('assets/icons/font-awesome-4.7.0/css/font-awesome.min.css')}}">
        
        <!-- Custom Icon Font -->
        <link rel="stylesheet" href="{{asset('assets/fonts/flaticon.css')}}">
        <!-- Bootstrap-->
        <link rel="stylesheet" href="{{asset('assets/plugins/css/bootstrap.min.css')}}">
        <!-- Fancybox-->
        <link rel="stylesheet" href="{{asset('assets/plugins/css/jquery.fancybox.min.css')}}">
        <!-- Animation -->
        <link rel="stylesheet" href="{{asset('assets/plugins/css/animate.css')}}">
        <!-- owl -->
        <link rel="stylesheet" href="{{asset('assets/plugins/css/owl.css')}}">
        <!--flexslider-->
        <link rel="stylesheet" href="{{asset('assets/plugins/css/flexslider.min.css')}}">
        <!-- selectize -->
        <link rel="stylesheet" href="{{asset('assets/plugins/css/selectize.css')}}">
        <link rel="stylesheet" href="{{asset('assets/plugins/css//selectize.bootstrap3.css')}}">
        <link rel="stylesheet" href="{{asset('assets/plugins/css/jquery-ui.min.css')}}">
        <!--dropdown -->
        <link rel="stylesheet" href="{{asset('assets/plugins/css/bootstrap-dropdownhover.min.css')}}">
        <!-- mobile nav-->
        <link rel="stylesheet" href="{{asset('assets/plugins/css/meanmenu.css')}}">

        <!-- COUSTOM CSS link  -->
        <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">
        @yield('css')
        <!--[if lt IE 9]>
            <script src="js/plagin-js/html5shiv.js"></script>
            <script src="js/plagin-js/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <!--
        |========================
        |  HEADER
        |========================
        -->
        @yield('header')

        <!--
        |========================
        |  content
        |========================
        -->
        @yield('content')
        
        <!--
        |========================
        |  FOOTER
        |========================
        -->
        @yield('footer')
        <!--
        |========================
        |      Script
        |========================
        -->
        <!-- jquery -->
        <script src="{{asset('assets/plugins/js/jquery-1.11.3.min.js')}}"></script>
        <!-- Bootstrap -->
        <script src="{{asset('assets/plugins/js/bootstrap.min.js')}}"></script>
        <!-- mean menu nav-->
        <script src="{{asset('assets/plugins/js/meanmenu.js')}}"></script>
        <!-- ajaxchimp -->
        <script src="{{asset('assets/plugins/js/jquery.ajaxchimp.min.js')}}"></script>
        <!-- wow -->
        <script src="{{asset('assets/plugins/js/wow.min.js')}}"></script>
        <!-- Owl carousel-->
        <script src="{{asset('assets/plugins/js/owl.carousel.js')}}"></script>
        <!--flexslider-->
        <script src="{{asset('assets/plugins/js/jquery.flexslider-min.js')}}"></script>
        <!--dropdownhover-->
        <script src="{{asset('assets/plugins/js/bootstrap-dropdownhover.min.js')}}"></script>
        <!--jquery-ui.min-->
        <script src="{{asset('assets/plugins/js/jquery-ui.min.js')}}"></script>
        <!--validator -->
        <script src="{{asset('assets/plugins/js/validator.min.js')}}"></script>
        <!--smooth scroll-->
        <script src="{{asset('assets/plugins/js/smooth-scroll.js')}}"></script>
        <!-- Fancybox js-->
        <script src="{{asset('assets/plugins/js/jquery.fancybox.min.js')}}"></script>
        <!-- SELECTIZE-->
        <script src="{{asset('assets/plugins/js/standalone/selectize.js')}}"></script>
        <!-- init -->
        <script src="{{asset('assets/js/init.js')}}"></script>
        @yield('script')
    </body>
</html>