<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'title'            => $faker->realText(100),
        'author_id'        => rand(1,3),
        'category_id'        => rand(1,2),
        'seo_title'        =>  $faker->realText(50),
        'excerpt'          => $faker->realText(200),
        'body'             => '<p>'.$faker->realText(1500).'</p>',
        'image'            => 'posts/November2017/Emerald-Maldives-Resort-Spa-1-1-1068x712.jpg',
        'slug'             => $faker->unique()->safeEmail,
        'meta_description' => 'This is the meta description',
        'meta_keywords'    => 'keyword1, keyword2, keyword3',
        'status'           => 'PUBLISHED',
        'featured'         => 1,
    ];
});
