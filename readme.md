# To see latest codes visit the "dev" brunch #

## About Safetravels.life

safetravels.life is going to be an affiliate store website along with blogs. currently, it's in the development phase.

## Key Features 

1. Allow users to compare prices between different e-commerce sites. 

2. User/ Affiliate marketers can upload their product link to promote. 

3. Will Have a complete blog. 

4. Facebook comments and social media share options.

## Demo Link

To see the demo of the current stage - **[click here](https://safetravels.000webhostapp.com)** . <br>
To visit admin panel click **[here](https://safetravels.000webhostapp.com/admin/login/)** . <br>
Username: admin@gmail.com <br>
Password: safetravels12


