<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Post;
use App\User;

class BlogController extends Controller
{
    public function index()
    {
        $featureds = Post::featured()->orderBy('created_at', 'desc')->take(6)->get();
        $posts = Post::latest()->take(12)->get();
        return view('blog.index', compact('posts', 'featureds'));
    }
    
    public function show($slug)
    {
        $posts = Post::orderBy('created_at', 'desc')->take(5)->get();
        $post = Post::where('slug', '=', $slug)->firstOrFail();
        return view('blog.post', compact('post','posts'));
    }

    public function categoryStories($slug)
    {
        $id = Category::where('slug','=',$slug)->first()->id;
        $posts = Category::find($id)->posts()->paginate(5);
        return view('blog.lists',compact('posts'));
    }

    public function authorsStories($name)
    {
        $id = User::where('name','=',$name)->first()->id;
        $posts = Post::where('author_id','=',$id)->orderBy('created_at', 'desc')->paginate(5);
        return view('blog.lists',compact('posts'));
    }

    public function featuredStories()
    {
        $posts = Post::featured()->orderBy('created_at', 'desc')->paginate(5);
        return view('blog.lists',compact('posts'));
    }

    public function search(Request $request)
    {
        if($request->category=='All Categories'){
            $posts = Post::searchKeyword($request->keyword)->paginate(5);
        }else{
            $id = Category::where('name','=',$request->category)->first()->id;
            $posts = Post::searchCategory($id)->searchKeyword($request->keyword)->paginate(5);
        }
        
        return view('welcome',compact('posts')); 
    }
}
