<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function childs()
    {
        return $this->hasMany('App\Category', 'parent_id');
    }

    public static function getChilds($id)
    {
        $order = Self::findOrFail($id)->order;
        $childs = Self::find($id)->childs;
        if($order==1 && count($childs)>0){
            return $childs;
        }else{
            return null;
        } 
    }
    public function posts()
    {
        return $this->hasMany('App\Post')
            ->orderBy('created_at', 'DESC');
    }
}
