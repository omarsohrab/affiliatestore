<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Post extends Model
{
    public function author()
    {
        return $this->belongsTo('App\User', 'author_id', 'id');
    }
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function getCreatedAtAttribute($value){
        $end = Carbon::parse($value);        
        return $end->diffForHumans();
    }

    public function scopePublished(Builder $query)
    {
        return $query->where('status', '=', static::PUBLISHED);
    }

    public function scopeFeatured($query)
    {
        return $query->where('featured', '=', 1);
    }

    public function scopeSearchKeyword($query, $key)
    {
        return $query->where('title', 'like', '%'.$key.'%')
                     ->orWhere('excerpt', 'like', '%'.$key.'%');
    }
    public function scopeSearchCategory($query, $category)
    {
        return $query->where('category_id', '=',$category);
    }
}
